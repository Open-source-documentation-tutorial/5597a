n=0:1:14;
wR=ones(1,15);% 编写矩形窗
hd=sin(0.25*pi*(n-7+eps))./(pi*(n-7+eps));%读入hd（n）函数
h1=hd.*wR;%计算h（n）
N=64; 
H1=fft(h1,N);%调用子程序计算H（k）
n=0:N-1;w=2*pi/64*n;subplot(2,2,1);
plot(w,fftshift(20*log10((abs(H1)))));%画幅度曲线
grid on
xlabel('w/rad')
ylabel('20lg|H(jw)|/dB')
title('幅度曲线和相频曲线（n=15）');
n=0:N-1;w=2*pi/64*n;subplot(2,2,3);
plot(w,fftshift(unwrap(phase(H1))));%画相频曲线 
grid
xlabel('w/rad')
clear all;
n=0:1:32;
wR=ones(1,33);% 编写矩形窗
hd=sin(0.25*pi*(n-16+eps))./(pi*(n-16+eps));%读入hd（n）函数
h1=hd.*wR;%计算h（n）
N=64;
H1=fft(h1,N);%调用子程序计算H（k）
n=0:N-1;w=2*pi/64*n;subplot(2,2,2);
plot(w,fftshift(20*log10((abs(H1)))));%画幅度曲线
grid on
xlabel('w/rad')
ylabel('20lg|H(jw)|/dB')
title('幅度曲线和相频曲线（n=15）');
n=0:N-1;w=2*pi/64*n;subplot(2,2,4);
plot(w,fftshift(unwrap(phase(H1))));%画相频曲线 
grid
xlabel('w/rad')
